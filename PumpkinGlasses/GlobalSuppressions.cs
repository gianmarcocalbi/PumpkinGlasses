﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Stile", "IDE1006:Stili di denominazione", Justification = "<In sospeso>", Scope = "member", Target = "~M:PumpkinGlasses.Window.label1_Click(System.Object,System.EventArgs)")]

