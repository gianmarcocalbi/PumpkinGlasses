﻿namespace PumpkinGlasses
{
    partial class Window
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.textBoxZipPath = new System.Windows.Forms.TextBox();
            this.labelZipPath = new System.Windows.Forms.Label();
            this.btnZipPath = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnAbort = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.openZipFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveZipFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.labelLog = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.textBoxCodiceModello = new System.Windows.Forms.TextBox();
            this.labelPIVA = new System.Windows.Forms.Label();
            this.labelCodiceModello = new System.Windows.Forms.Label();
            this.textBoxPIVA = new System.Windows.Forms.TextBox();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.labelAzienda = new System.Windows.Forms.Label();
            this.textBoxAzienda = new System.Windows.Forms.TextBox();
            this.labelAnnoMeseManuali = new System.Windows.Forms.Label();
            this.radioBtnManualMYtrue = new System.Windows.Forms.RadioButton();
            this.radioBtnManualMYfalse = new System.Windows.Forms.RadioButton();
            this.labelMese = new System.Windows.Forms.Label();
            this.numericUpDownMese = new System.Windows.Forms.NumericUpDown();
            this.labelAnno = new System.Windows.Forms.Label();
            this.numericUpDownAnno = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMese)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnno)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxZipPath
            // 
            this.textBoxZipPath.AllowDrop = true;
            this.textBoxZipPath.Location = new System.Drawing.Point(12, 22);
            this.textBoxZipPath.Name = "textBoxZipPath";
            this.textBoxZipPath.Size = new System.Drawing.Size(430, 20);
            this.textBoxZipPath.TabIndex = 0;
            this.textBoxZipPath.TextChanged += new System.EventHandler(this.textBoxZipPath_TextChanged);
            // 
            // labelZipPath
            // 
            this.labelZipPath.AutoSize = true;
            this.labelZipPath.Location = new System.Drawing.Point(12, 6);
            this.labelZipPath.Name = "labelZipPath";
            this.labelZipPath.Size = new System.Drawing.Size(116, 13);
            this.labelZipPath.TabIndex = 1;
            this.labelZipPath.Text = "Seleziona archivio ZIP:";
            // 
            // btnZipPath
            // 
            this.btnZipPath.Location = new System.Drawing.Point(448, 22);
            this.btnZipPath.Name = "btnZipPath";
            this.btnZipPath.Size = new System.Drawing.Size(24, 20);
            this.btnZipPath.TabIndex = 1;
            this.btnZipPath.Text = "...";
            this.btnZipPath.UseVisualStyleBackColor = true;
            this.btnZipPath.Click += new System.EventHandler(this.btnZipPath_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(163, 218);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(84, 30);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "Avvia";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnAbort
            // 
            this.btnAbort.Enabled = false;
            this.btnAbort.Location = new System.Drawing.Point(253, 218);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(82, 30);
            this.btnAbort.TabIndex = 6;
            this.btnAbort.Text = "Interrompi";
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.richTextBoxLog.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.richTextBoxLog.Location = new System.Drawing.Point(12, 271);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.ReadOnly = true;
            this.richTextBoxLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBoxLog.Size = new System.Drawing.Size(460, 100);
            this.richTextBoxLog.TabIndex = 8;
            this.richTextBoxLog.TabStop = false;
            this.richTextBoxLog.Text = "";
            // 
            // openZipFileDialog
            // 
            this.openZipFileDialog.DefaultExt = "zip";
            this.openZipFileDialog.FileName = "openZipFileDialog";
            this.openZipFileDialog.Filter = "Archivio ZIP | *.zip";
            // 
            // labelLog
            // 
            this.labelLog.AutoSize = true;
            this.labelLog.Enabled = false;
            this.labelLog.Location = new System.Drawing.Point(15, 255);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(28, 13);
            this.labelLog.TabIndex = 9;
            this.labelLog.Text = "Log:";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 377);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(460, 23);
            this.progressBar.TabIndex = 10;
            // 
            // textBoxCodiceModello
            // 
            this.textBoxCodiceModello.Location = new System.Drawing.Point(104, 115);
            this.textBoxCodiceModello.Name = "textBoxCodiceModello";
            this.textBoxCodiceModello.Size = new System.Drawing.Size(368, 20);
            this.textBoxCodiceModello.TabIndex = 4;
            this.textBoxCodiceModello.TextChanged += new System.EventHandler(this.textBoxCodiceModello_TextChanged);
            // 
            // labelPIVA
            // 
            this.labelPIVA.AutoSize = true;
            this.labelPIVA.Location = new System.Drawing.Point(58, 88);
            this.labelPIVA.Name = "labelPIVA";
            this.labelPIVA.Size = new System.Drawing.Size(40, 13);
            this.labelPIVA.TabIndex = 13;
            this.labelPIVA.Text = "P. IVA:";
            // 
            // labelCodiceModello
            // 
            this.labelCodiceModello.AutoSize = true;
            this.labelCodiceModello.Location = new System.Drawing.Point(15, 118);
            this.labelCodiceModello.Name = "labelCodiceModello";
            this.labelCodiceModello.Size = new System.Drawing.Size(83, 13);
            this.labelCodiceModello.TabIndex = 14;
            this.labelCodiceModello.Text = "Codice Modello:";
            // 
            // textBoxPIVA
            // 
            this.textBoxPIVA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxPIVA.Location = new System.Drawing.Point(104, 85);
            this.textBoxPIVA.MaxLength = 11;
            this.textBoxPIVA.Name = "textBoxPIVA";
            this.textBoxPIVA.Size = new System.Drawing.Size(368, 20);
            this.textBoxPIVA.TabIndex = 3;
            this.textBoxPIVA.TextChanged += new System.EventHandler(this.textBoxPIVA_TextChanged);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // labelAzienda
            // 
            this.labelAzienda.AutoSize = true;
            this.labelAzienda.Location = new System.Drawing.Point(50, 58);
            this.labelAzienda.Name = "labelAzienda";
            this.labelAzienda.Size = new System.Drawing.Size(48, 13);
            this.labelAzienda.TabIndex = 15;
            this.labelAzienda.Text = "Azienda:";
            // 
            // textBoxAzienda
            // 
            this.textBoxAzienda.Location = new System.Drawing.Point(104, 55);
            this.textBoxAzienda.Name = "textBoxAzienda";
            this.textBoxAzienda.Size = new System.Drawing.Size(368, 20);
            this.textBoxAzienda.TabIndex = 2;
            this.textBoxAzienda.TextChanged += new System.EventHandler(this.textBoxAzienda_TextChanged);
            // 
            // labelAnnoMeseManuali
            // 
            this.labelAnnoMeseManuali.AutoSize = true;
            this.labelAnnoMeseManuali.Location = new System.Drawing.Point(15, 148);
            this.labelAnnoMeseManuali.Name = "labelAnnoMeseManuali";
            this.labelAnnoMeseManuali.Size = new System.Drawing.Size(191, 13);
            this.labelAnnoMeseManuali.TabIndex = 16;
            this.labelAnnoMeseManuali.Text = "Impostare Mese e Anno manualmente?";
            // 
            // radioBtnManualMYtrue
            // 
            this.radioBtnManualMYtrue.AutoSize = true;
            this.radioBtnManualMYtrue.Location = new System.Drawing.Point(218, 147);
            this.radioBtnManualMYtrue.Name = "radioBtnManualMYtrue";
            this.radioBtnManualMYtrue.Size = new System.Drawing.Size(34, 17);
            this.radioBtnManualMYtrue.TabIndex = 5;
            this.radioBtnManualMYtrue.Text = "Si";
            this.radioBtnManualMYtrue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioBtnManualMYtrue.UseVisualStyleBackColor = true;
            this.radioBtnManualMYtrue.CheckedChanged += new System.EventHandler(this.radioBtnManualMYtrue_CheckedChanged);
            // 
            // radioBtnManualMYfalse
            // 
            this.radioBtnManualMYfalse.AutoSize = true;
            this.radioBtnManualMYfalse.Checked = true;
            this.radioBtnManualMYfalse.Location = new System.Drawing.Point(259, 147);
            this.radioBtnManualMYfalse.Name = "radioBtnManualMYfalse";
            this.radioBtnManualMYfalse.Size = new System.Drawing.Size(39, 17);
            this.radioBtnManualMYfalse.TabIndex = 6;
            this.radioBtnManualMYfalse.TabStop = true;
            this.radioBtnManualMYfalse.Text = "No";
            this.radioBtnManualMYfalse.UseVisualStyleBackColor = true;
            // 
            // labelMese
            // 
            this.labelMese.AutoSize = true;
            this.labelMese.Location = new System.Drawing.Point(94, 180);
            this.labelMese.Name = "labelMese";
            this.labelMese.Size = new System.Drawing.Size(36, 13);
            this.labelMese.TabIndex = 19;
            this.labelMese.Text = "Mese:";
            this.labelMese.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDownMese
            // 
            this.numericUpDownMese.Enabled = false;
            this.numericUpDownMese.Location = new System.Drawing.Point(136, 178);
            this.numericUpDownMese.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownMese.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMese.Name = "numericUpDownMese";
            this.numericUpDownMese.Size = new System.Drawing.Size(41, 20);
            this.numericUpDownMese.TabIndex = 7;
            this.numericUpDownMese.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelAnno
            // 
            this.labelAnno.AutoSize = true;
            this.labelAnno.Location = new System.Drawing.Point(230, 180);
            this.labelAnno.Name = "labelAnno";
            this.labelAnno.Size = new System.Drawing.Size(35, 13);
            this.labelAnno.TabIndex = 22;
            this.labelAnno.Text = "Anno:";
            // 
            // numericUpDownAnno
            // 
            this.numericUpDownAnno.Enabled = false;
            this.numericUpDownAnno.Location = new System.Drawing.Point(271, 178);
            this.numericUpDownAnno.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.numericUpDownAnno.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.numericUpDownAnno.Name = "numericUpDownAnno";
            this.numericUpDownAnno.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownAnno.TabIndex = 8;
            this.numericUpDownAnno.Value = new decimal(new int[] {
            2017,
            0,
            0,
            0});
            // 
            // Window
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 411);
            this.Controls.Add(this.numericUpDownAnno);
            this.Controls.Add(this.labelAnno);
            this.Controls.Add(this.numericUpDownMese);
            this.Controls.Add(this.labelMese);
            this.Controls.Add(this.radioBtnManualMYfalse);
            this.Controls.Add(this.radioBtnManualMYtrue);
            this.Controls.Add(this.labelAnnoMeseManuali);
            this.Controls.Add(this.textBoxAzienda);
            this.Controls.Add(this.labelAzienda);
            this.Controls.Add(this.labelCodiceModello);
            this.Controls.Add(this.labelPIVA);
            this.Controls.Add(this.textBoxCodiceModello);
            this.Controls.Add(this.textBoxPIVA);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.labelLog);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.btnAbort);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnZipPath);
            this.Controls.Add(this.labelZipPath);
            this.Controls.Add(this.textBoxZipPath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MakeDesc";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMese)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnno)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxZipPath;
        private System.Windows.Forms.Label labelZipPath;
        private System.Windows.Forms.Button btnZipPath;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.OpenFileDialog openZipFileDialog;
        private System.Windows.Forms.SaveFileDialog saveZipFileDialog;
        private System.Windows.Forms.Label labelLog;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TextBox textBoxCodiceModello;
        private System.Windows.Forms.Label labelPIVA;
        private System.Windows.Forms.Label labelCodiceModello;
        private System.Windows.Forms.TextBox textBoxPIVA;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Label labelAzienda;
        private System.Windows.Forms.TextBox textBoxAzienda;
        private System.Windows.Forms.Label labelAnnoMeseManuali;
        private System.Windows.Forms.RadioButton radioBtnManualMYtrue;
        private System.Windows.Forms.RadioButton radioBtnManualMYfalse;
        private System.Windows.Forms.Label labelMese;
        private System.Windows.Forms.NumericUpDown numericUpDownMese;
        private System.Windows.Forms.Label labelAnno;
        private System.Windows.Forms.NumericUpDown numericUpDownAnno;
    }
}

