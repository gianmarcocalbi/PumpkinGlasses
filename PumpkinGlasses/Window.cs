﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Windows.Forms;

namespace PumpkinGlasses {
    public partial class Window : Form {
        private string defaultTxtFileNameWithoutExt = "attribute_file";
        private string defaultTxtFileNameWithExt = "attribute_file.znf";
        private GuiConsole guiConsole;
        private string zipFileName = "";
        private string zipPath = "";
        private string azienda = "";
        private string PIVA = "";
        private string codiceModello = "";

        public Window () {
            InitializeComponent();
            guiConsole = new GuiConsole(richTextBoxLog);
            SetupDefault();
        }

        private void SetupDefault () {
            textBoxZipPath.Text = "";
            textBoxPIVA.Text = "";
            textBoxCodiceModello.Text = "";
            textBoxAzienda.Text = "";
            richTextBoxLog.Text = "";
            btnStart.Enabled = false;
            btnAbort.Enabled = false;
            radioBtnManualMYtrue.Checked = false;
            radioBtnManualMYfalse.Checked = true;
            numericUpDownAnno.Value = 2017;
            numericUpDownMese.Value = 1;
            guiConsole.NewLineLog("Pronto!");
        }

        private void SetupPrevious () {
            btnStart.Enabled = textBoxZipPath.Text != "" && Path.GetExtension(textBoxZipPath.Text).Equals(".zip");
            btnAbort.Enabled = false;
            progressBar.Value = 0;
            guiConsole.NewLineLog("Pronto per una nuova esecuzione!");
        }

        private void btnZipPath_Click (object sender, EventArgs e) {
            DialogResult result = openZipFileDialog.ShowDialog();
            if (result == DialogResult.OK) {
                zipPath = openZipFileDialog.FileName;
                zipFileName = Path.GetFileName(zipPath);
                textBoxZipPath.Text = zipPath;
                guiConsole.NewLineLog("Selezionato archivio " + zipFileName);
                btnStart.Enabled = true;
            }
        }

        private void btnStart_Click (object sender, EventArgs e) {
            if (true) {
                DialogResult dialogResult = MessageBox.Show(String.Format("Archivio ZIP: {0}\nAzienda: {1}\nP.IVA: {2}\nCodice Modello: {3}", zipFileName, textBoxAzienda.Text, textBoxPIVA.Text, textBoxCodiceModello.Text), "Avviare l'estrazione?", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK) {
                    //disable some forms, buttons, textboxes etc..
                    guiConsole.NewLineLog("Task avviato");
                    btnStart.Enabled = false;
                    btnAbort.Enabled = true;
                    backgroundWorker.RunWorkerAsync();
                } else if (dialogResult == DialogResult.Cancel) {
                    //do something else
                }
            } else {
                //cannot start
                MessageBox.Show("");
            }


            /*
            nomefile1ConEstensione;CFdalNomeDelFile;PIVAdaTextBox;CodiceModelloDaTextbox;072016(meseAnno);
            nomefile2;....;

            FILENAME=nome.pdf;CFISC=piva;COFISCD=cf;ANNO=2017;MESE=03;DENAZI=nomeAzienda;HRZ_MODEL=codModello
            */
        }


        private void backgroundWorker_DoWork (object sender, System.ComponentModel.DoWorkEventArgs e) {
            List<string> strList = new List<string>();
            //string tempPath = Path.GetTempPath();

            Random rnd = new Random();
            if (backgroundWorker.CancellationPending) {
                e.Cancel = true;
                return;
            }
            backgroundWorker.ReportProgress(rnd.Next(20));
            try {
                using (ZipArchive archive = ZipFile.OpenRead(zipPath)) {
                    double step = 60.0 / archive.Entries.Count;
                    int i = 0;
                    foreach (ZipArchiveEntry entry in archive.Entries) {
                        if (backgroundWorker.CancellationPending) {
                            e.Cancel = true;
                            return;
                        }
                        string pdfname = entry.FullName;
                        string pdfnameWithoutExt = Path.GetFileNameWithoutExtension(pdfname);
                        string CF = pdfnameWithoutExt.Substring(0, 16);
                        string month, year;
                        if (radioBtnManualMYtrue.Checked) {
                            month = numericUpDownMese.Value.ToString();
                            if (month.Length == 1) {
                                month = "0" + month;
                            }
                            year = numericUpDownAnno.Value.ToString();
                        } else {
                            string monthYear = pdfnameWithoutExt.Substring(pdfnameWithoutExt.Length - 6);
                            month = monthYear.Substring(0, 2);
                            year = monthYear.Substring(2);
                        }

                        //FILENAME=BLLLMC63A65F205D_032017.pdf;CFISC=00743840159;COFISCD=BLLLMC63A65F205D;ANNO=2017;MESE=03;DENAZI=AVVENIRE S.P.A.;HRZ_MODEL=UIMAVVENIRE
                        strList.Add(String.Format("FILENAME={0};CFISC={1};COFISCD={2};ANNO={3};MESE={4};DENAZI={5};HRZ_MODEL={6}", pdfname, PIVA, CF, year, month, azienda, codiceModello));
                        backgroundWorker.ReportProgress(20 + (int) (step * i));
                        ++i;
                        Thread.Sleep(10);
                    }
                }
                if (backgroundWorker.CancellationPending) {
                    e.Cancel = true;
                    return;
                }
                backgroundWorker.ReportProgress(80);
                strList.Sort();
                if (backgroundWorker.CancellationPending) {
                    e.Cancel = true;
                    return;
                }
                e.Result = String.Join("\n", strList);
            } catch {
                e.Cancel = true;
                MessageBox.Show("Errore nella lettura dell'archivio in input.");
            }
        }

        private void backgroundWorker_ProgressChanged (object sender, System.ComponentModel.ProgressChangedEventArgs e) {
            progressBar.Value = e.ProgressPercentage;
        }

        private void backgroundWorker_RunWorkerCompleted (object sender, System.ComponentModel.RunWorkerCompletedEventArgs e) {
            guiConsole.NewLineLog("Il background worker ha terminato il suo task");
            if (!e.Cancelled) {
                string txtContent = e.Result.ToString();
                saveZipFileDialog.FileName = defaultTxtFileNameWithExt;
                saveZipFileDialog.Filter = "ZNF file (*.znf)|*.znf";

                if (saveZipFileDialog.ShowDialog() == DialogResult.OK) {
                    string txtPath = Path.GetFullPath(saveZipFileDialog.FileName);
                    progressBar.Value = 90;
                    using (var sw = new StreamWriter(txtPath)) {
                        sw.Write(txtContent);
                        progressBar.Value = 100;
                        sw.Close();
                        guiConsole.NewLineLog("File salvato correttamente in " + txtPath);
                        MessageBox.Show("Esecuzione terminata con successo.", "Fine!");
                    }
                } else {
                    guiConsole.NewLineLog("Salvataggio file annullato, file NON salvato");
                }
            } else {
                guiConsole.NewLineError("Task cancellato");
            }
            SetupPrevious();
        }

        private void btnAbort_Click (object sender, EventArgs e) {
            if (backgroundWorker.IsBusy) {
                backgroundWorker.CancelAsync();
            }
        }

        private void textBoxZipPath_TextChanged (object sender, EventArgs e) {
            if (textBoxZipPath.Text == "" || !Path.GetExtension(textBoxZipPath.Text).Equals(".zip")) {
                btnStart.Enabled = false;
            } else {
                btnStart.Enabled = true;
            }
        }

        private void textBoxPIVA_TextChanged (object sender, EventArgs e) {
            PIVA = textBoxPIVA.Text;
        }

        private void textBoxCodiceModello_TextChanged (object sender, EventArgs e) {
            codiceModello = textBoxCodiceModello.Text;
        }

        private void textBoxAzienda_TextChanged (object sender, EventArgs e) {
            azienda = textBoxAzienda.Text;
        }

        private void radioBtnManualMYtrue_CheckedChanged (object sender, EventArgs e) {
            numericUpDownAnno.Enabled = numericUpDownMese.Enabled = radioBtnManualMYtrue.Checked;
        }
    }

    public partial class GuiConsole {
        RichTextBox richTextBoxLog;

        public GuiConsole (RichTextBox richTextBoxLog) {
            this.richTextBoxLog = richTextBoxLog;
        }

        public void Log (String msg) {
            richTextBoxLog.AppendText(msg);
        }

        public void NewLineLog (String msg) {
            Log(msg + "\n");
        }

        public void Success (String str) {
            richTextBoxLog.SelectionStart = richTextBoxLog.TextLength;
            richTextBoxLog.SelectionLength = 0;
            richTextBoxLog.SelectionColor = Color.Green;
            richTextBoxLog.AppendText(str);
            richTextBoxLog.SelectionColor = richTextBoxLog.ForeColor;
        }

        public void NewLineSuccess (String str) {
            Success(str + "\n");
        }

        public void Error (String error) {
            richTextBoxLog.SelectionStart = richTextBoxLog.TextLength;
            richTextBoxLog.SelectionLength = 0;
            richTextBoxLog.SelectionColor = Color.Red;
            richTextBoxLog.AppendText(error);
            richTextBoxLog.SelectionColor = richTextBoxLog.ForeColor;
        }

        public void NewLineError (String error) {
            Error(error + "\n");
        }
    }
}